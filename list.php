<?php
//database code connect from bd.php
require_once('db.php');
//Selecting Data ( To Fetch data as object )
$query = "SELECT * FROM code_list";
$result = $database->query($query);

?>




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Resource List</title>
</head>

<body>
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #000000;
            background-color: #ff7700;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1600 900'%3E%3Cpolygon fill='%23cc0000' points='957 450 539 900 1396 900'/%3E%3Cpolygon fill='%23aa0000' points='957 450 872.9 900 1396 900'/%3E%3Cpolygon fill='%23d6002b' points='-60 900 398 662 816 900'/%3E%3Cpolygon fill='%23b10022' points='337 900 398 662 816 900'/%3E%3Cpolygon fill='%23d9004b' points='1203 546 1552 900 876 900'/%3E%3Cpolygon fill='%23b2003d' points='1203 546 1552 900 1162 900'/%3E%3Cpolygon fill='%23d3006c' points='641 695 886 900 367 900'/%3E%3Cpolygon fill='%23ac0057' points='587 900 641 695 886 900'/%3E%3Cpolygon fill='%23c4008c' points='1710 900 1401 632 1096 900'/%3E%3Cpolygon fill='%239e0071' points='1710 900 1401 632 1365 900'/%3E%3Cpolygon fill='%23aa00aa' points='1210 900 971 687 725 900'/%3E%3Cpolygon fill='%23880088' points='943 900 1210 900 971 687'/%3E%3C/svg%3E");
background-attachment: fixed;
background-size: cover;
font-family: 'Amatic SC', cursive;
        }

h1{
    text-align: center;
            color: black;
            margin-bottom: 5px;
            font-size: 50px;
            letter-spacing: 2.5px;
}

span {
            color:  white;
        }
  
  
    </style>
    <div class="container">
        <div class="jumbtorn">

            <h1>R<span>e</span>s<span>o</span>u<span>r</span>c<span>e</span> U<span>r</span>l <spanL></span><span>L</span>i<span>s</span>t</h1>

            <table class="table table-striped table-light text-dark">
                <thead>
                    <tr class="col text-warning">
                        
                        <th scope="col">Name</th>
                        <th scope="col">Topics</th>
                        <th scope="col">Url</th>
                       

                    </tr>
                </thead>
                <tbody>
                    <?php /*using loop in table -> using fatchobject key*/ while ($list = $result->fetchObject()) {
                        ?>
                        <tr>
                            <td><?php /*for catching database colum data*/ echo $list->name ?></td>
                            <td><?php /*for catching database colum data*/ echo $list->topics ?></td>
                            <td> <a href="<?php /*for catching database colum data and add link tag*/ echo $list->url ?>" target="_blank" class="text-dark">Click Here</a>
 </td>
                        </tr>
                    <?php /*end of loop*/ } ?>
                </tbody>
            </table>
        </div>
    </div>

</body>

</html>