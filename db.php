<?php
//database connection
try {

    $database = new PDO('mysql:host=localhost;  port=3306; dbname=code', 'root', '');
    //when you need to see your error then you need to use this code--->
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error) {
    echo 'Database Connection Fail';
}
