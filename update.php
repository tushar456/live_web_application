<?php
//User submit font-side-code
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC&display=swap" rel="stylesheet">

    <title>Done</title>
</head>

<body>
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #ff7700;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1600 900'%3E%3Cpolygon fill='%23cc0000' points='957 450 539 900 1396 900'/%3E%3Cpolygon fill='%23aa0000' points='957 450 872.9 900 1396 900'/%3E%3Cpolygon fill='%23d6002b' points='-60 900 398 662 816 900'/%3E%3Cpolygon fill='%23b10022' points='337 900 398 662 816 900'/%3E%3Cpolygon fill='%23d9004b' points='1203 546 1552 900 876 900'/%3E%3Cpolygon fill='%23b2003d' points='1203 546 1552 900 1162 900'/%3E%3Cpolygon fill='%23d3006c' points='641 695 886 900 367 900'/%3E%3Cpolygon fill='%23ac0057' points='587 900 641 695 886 900'/%3E%3Cpolygon fill='%23c4008c' points='1710 900 1401 632 1096 900'/%3E%3Cpolygon fill='%239e0071' points='1710 900 1401 632 1365 900'/%3E%3Cpolygon fill='%23aa00aa' points='1210 900 971 687 725 900'/%3E%3Cpolygon fill='%23880088' points='943 900 1210 900 971 687'/%3E%3C/svg%3E");
background-attachment: fixed;
background-size: cover;


        
            background-attachment: fixed;
            background-size: cover;
        }

        .header {
            height: 100vh;
            background-image: linear-gradient(rgba(0, 0, 0, 0, 7), rgba(0, 0, 0, 0, 7));
            background-size: cover;
            background-position: center;
            display: flex;
            align-items: center;
            font-family: cursive;
            justify-content: center;
            font-family: 'Amatic SC', cursive;
        }

        h1  {
            text-align: center;
            color: black;
            margin-bottom: 5px;
            font-size: 50px;
            letter-spacing: 2.5px;
        }
        h2  {
            text-align: center;
            color: black;
            margin-bottom: 5px;
            font-size: 30px;
            letter-spacing: 2.5px;
        }

        span {
            color:  white;
        }

        .input {
            height: 15px;
            padding: 10px;
            width: 215px;
            font-family: cursive;
        }

        .submit {
            height: 40px;
            width: 115px;
            background: black;
            border: none;
            font-family: cursive;
            color: white;

        }

        .submit:hover {
            cursor: pointer;
            background: orange;
        }

        .form {
            background: rgba(255, 255, 255, 0.5);
            padding: 25px 40px;
        }
    </style>
    <div class="header">
        <form method="" action="">
                <h1>U<span>r</span>l   <span>S</span>u<span>b</span>m i<span>t</span> t<span>e</span>d</h1>      
    </div>
</body>

</html>