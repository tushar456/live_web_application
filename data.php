<?php
//database code connect from bd.php
require_once('db.php');

//user input post method
$name = $_POST['name'];
$topics = $_POST['topics'];
$url = $_POST['url'];


// insert data
$resource = $database->prepare("INSERT INTO code_list (name, topics, url) 
VALUES (:name, :topics, :url)");

$resource->bindParam(":name", $name, PDO::PARAM_STR);
$resource->bindParam(":topics", $topics, PDO::PARAM_STR);
$resource->bindParam(":url", $url, PDO::PARAM_STR);
$resource->execute();

//if else condition for user data
if ($resource == True) {
    header("location: update.php");
} else {
    echo 'Please Enter You Right Information';
}
