-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 01:47 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `code`
--
CREATE DATABASE IF NOT EXISTS `code` DEFAULT CHARACTER SET utf16 COLLATE utf16_unicode_ci;
USE `code`;

-- --------------------------------------------------------

--
-- Table structure for table `code_list`
--

CREATE TABLE `code_list` (
  `name` varchar(60) COLLATE utf16_unicode_ci NOT NULL,
  `topics` varchar(70) COLLATE utf16_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf16_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `code_list`
--

INSERT INTO `code_list` (`name`, `topics`, `url`) VALUES
('Nizam', 'PHP', 'https://bit.ly/2Ju9EsJ'),
('TUSHAR', 'Bootstrap', 'https://getbootstrap.com/docs/4.0/getting-started/introduction/'),
('MERAJ', 'Laravel ', 'laravel.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
